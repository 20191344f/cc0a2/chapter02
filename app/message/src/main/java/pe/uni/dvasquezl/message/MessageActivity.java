package pe.uni.dvasquezl.message;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;

public class MessageActivity extends AppCompatActivity {
    Button buttonToast, buttonSnackBar, buttonDialog;
    LinearLayout linearLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message);

        buttonToast = findViewById(R.id.button_message_1);
        buttonSnackBar = findViewById(R.id.button_message_2);
        buttonDialog = findViewById(R.id.button_message_3);
        linearLayout = findViewById(R.id.linearLayout);

        buttonToast.setOnClickListener(v -> Toast.makeText(getApplicationContext(), R.string.message_toast, Toast.LENGTH_LONG).show());
        buttonSnackBar.setOnClickListener(v -> Snackbar.make(linearLayout, R.string.message_snackbar, Snackbar.LENGTH_INDEFINITE).setAction(R.string.snackbar, v1 -> {
        }).show());
        buttonDialog.setOnClickListener(V -> {
            AlertDialog.Builder alerDialog = new AlertDialog.Builder(this);
            alerDialog
                    .setTitle(R.string.dialg_titulo)
                    .setMessage(R.string.dialg_text)
                    .setNegativeButton(R.string.no, (DialogInterface dialog, int wich) -> dialog.cancel())
                    .setPositiveButton(R.string.yes, (DialogInterface dialog, int wich) -> {
                        //something;
                    }).show();
            alerDialog.create();
        });
    }
}