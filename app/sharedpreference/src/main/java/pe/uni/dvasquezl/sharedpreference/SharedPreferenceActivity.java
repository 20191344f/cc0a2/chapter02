package pe.uni.dvasquezl.sharedpreference;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

public class SharedPreferenceActivity extends AppCompatActivity {

    EditText editTextName;
    EditText editTextMessage;
    Button button;
    CheckBox checkBox;
    SharedPreferences sharedPreferences;
    int counter = 0;

    String name;
    String message;
    Boolean isCheked;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shared_preference);

        editTextName = findViewById(R.id.edit_text_name);
        editTextMessage = findViewById(R.id.edit_text_message);
        button = findViewById(R.id.button);
        checkBox = findViewById(R.id.check_box);

        button.setOnClickListener(view -> {
            counter++;
            button.setText(String.valueOf(counter));
        });

        retrieveData();
    }

    @Override
    protected void onPause() {
        super.onPause();

        saveData();
    }

    private void saveData() {
        sharedPreferences = getSharedPreferences("saveData", Context.MODE_PRIVATE);
        name = editTextName.getText().toString();
        message = editTextMessage.getText().toString();
        isCheked = checkBox.isChecked();

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("key name", name);
        editor.putString("key message", message);
        editor.putInt("key counter", counter);
        editor.putBoolean("key isCheked", isCheked);

        editor.apply();

        Toast.makeText(getApplicationContext(), "Tus datos estan guardados", Toast.LENGTH_LONG).show();
    }

    private void retrieveData() {
        sharedPreferences = getSharedPreferences("saveData", Context.MODE_PRIVATE);

        name = sharedPreferences.getString("key name", null);
        message = sharedPreferences.getString("key message", null);
        counter = sharedPreferences.getInt("key counter", 0);
        isCheked = sharedPreferences.getBoolean("key isCheked", false);

        editTextName.setText(name);
        editTextMessage.setText(message);
        button.setText(String.valueOf(counter));
        checkBox.setChecked(isCheked);
    }
}