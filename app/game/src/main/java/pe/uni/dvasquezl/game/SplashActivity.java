package pe.uni.dvasquezl.game;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

@SuppressLint("CustomSplashScreen")
public class SplashActivity extends AppCompatActivity {

    ImageView imageViewSplash;
    TextView textViewSplash;

    Animation animationImage, animationText;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        imageViewSplash = findViewById(R.id.image_view_splash);
        textViewSplash = findViewById(R.id.text_view_splash);

        animationImage = AnimationUtils.loadAnimation(this, R.anim.image_animate);
        animationText = AnimationUtils.loadAnimation(this, R.anim.text_animation);

        imageViewSplash.setAnimation(animationImage);
        textViewSplash.setAnimation(animationText);

        new CountDownTimer(5000, 1000) {
            @Override
            public void onTick(long l) {

            }

            @Override
            public void onFinish() {
                Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        }.start();
    }
}