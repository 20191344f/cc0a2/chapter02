package pe.uni.dvasquezl.googlecodescanner;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCanceledListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.mlkit.common.MlKitException;
import com.google.mlkit.vision.barcode.common.Barcode;
import com.google.mlkit.vision.codescanner.GmsBarcodeScanner;
import com.google.mlkit.vision.codescanner.GmsBarcodeScannerOptions;
import com.google.mlkit.vision.codescanner.GmsBarcodeScanning;

import com.bosphere.filelogger.FL;

public class GoogleCodeScannerActivity extends AppCompatActivity {

    private static final String TAG = GoogleCodeScannerActivity.class.getSimpleName();
    TextView textViewScanner;
    Button buttonScanner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        FL.d(TAG, "onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_google_code_scanner);

        buttonScanner = findViewById(R.id.buttom_scanner);
        textViewScanner = findViewById(R.id.text_view_scanner);

        buttonScanner.setOnClickListener(view -> {
            GmsBarcodeScannerOptions options = new GmsBarcodeScannerOptions.Builder()
                    .setBarcodeFormats(Barcode.FORMAT_QR_CODE)
                    .build();

            GmsBarcodeScanner scanner = GmsBarcodeScanning.getClient(getApplicationContext(), options);

            scanner
                    .startScan()
                    .addOnSuccessListener(barcode -> {
                        FL.i("addOnSuccessListener");
                        textViewScanner.setText(getSuccessfulMessage((barcode)));
                    })
                    .addOnCanceledListener(() -> {
                        FL.i("addOnCanceledListener");
                        textViewScanner.setText(R.string.cancelled_option);
                    })
                    .addOnFailureListener(e -> {
                        FL.i("addOnFailureListener");
                        textViewScanner.setText(getFailureException((MlKitException) e));
                    });

        });

    }

    private String getSuccessfulMessage(Barcode barcode) {
        FL.i("getSuccessfulMessage");
        return String.format(
                getResources().getString(R.string.barcode_result),
                barcode.getRawValue(),
                barcode.getFormat(),
                barcode.getValueType());
    }

    private String getFailureException(MlKitException e) {
        FL.i("getFailureException");
        switch (e.getErrorCode()){
            case MlKitException.CODE_SCANNER_CANCELLED:
                return getString(R.string.error_code_scanner_cancelled);
            case MlKitException.UNKNOWN:
                return getString(R.string.error_code_unknown);
            default:
                return getString(R.string.error_default, e);
        }
    }
}