package pe.uni.dvasquezl.gridview;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.GridView;
import android.widget.Toast;

import java.util.ArrayList;

public class GridViewActivity extends AppCompatActivity {

    GridView gridView;
    ArrayList<String> text = new ArrayList<>();
    ArrayList<Integer> image = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grid_view);

        gridView = findViewById(R.id.grid_view);
        fillArray();

        GridAdapter gridAdapter = new GridAdapter(this, text, image);
        gridView.setAdapter(gridAdapter);

        gridView.setOnItemClickListener((parent, view, position, id) -> Toast.makeText(getApplicationContext(), text.get(position), Toast.LENGTH_LONG).show());
    }

    private void fillArray() {
        text.add("Bird");
        text.add("Cat");
        text.add("Chicken");
        text.add("Dog");
        text.add("Fish");
        text.add("Lion");
        text.add("Monkey");
        text.add("Rabbit");
        text.add("Sheep");

        image.add(R.drawable.cat);
        image.add(R.drawable.dog);
        image.add(R.drawable.dogbrow);
        image.add(R.drawable.cat);
        image.add(R.drawable.dog);
        image.add(R.drawable.dogbrow);
        image.add(R.drawable.cat);
        image.add(R.drawable.dog);
        image.add(R.drawable.dogbrow);
    }
}